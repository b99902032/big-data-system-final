import scala.collection._
import org.apache.spark.rdd._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._

final class myPageRankGIMV extends GIMV[Double]{
   
   def combine2(v1: Double, v2: Double): Double = {
     return v1 * v2
   }

   def combineAll(row: Seq[(Int,Double)]): Double = {
      var sum = 0.0
      row.foreach{case(col,v) => sum += v}
      return sum
   }

   def assign(original: RDD[(Int,Double)], newv: RDD[(Int,Double)]): RDD[(Int, Double)] = {
      return newv.mapValues(0.15+0.85*_)
   }

   def test(v1:Int,v2:Int){
      println("~~~~~" + v1 + "~~~~~~~~")
   }
   
}

object PageRank {
   def main(args: Array[String]){
      val lines = sc.textFile(args(1), 1)
      val iters = args(0).toInt
      val links = lines.map{
	 s => val parts = s.split("\\s+")
	 (parts(0).toInt, parts(1).toInt)
      }.distinct().groupByKey().cache()
      var ranks = links.mapValues(v=>1.0)
      val myPageRankGIMv = new myPageRankGIMV()
      val matrix = links.join(ranks).flatMap{case(src,(urls,rank)) => 
	 val size = urls.size
	 urls.map(url => (url, (src,rank/size)))
      }
      for(i <- 1 to iters) ranks = myPageRankGIMv.multiply(matrix,ranks).coalesce(12,false);
      //println("result:")
      //ranks.foreach(f => println(f))
      ranks.saveAsTextFile(args(2));
      
   }
}
