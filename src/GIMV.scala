import scala.collection._
import org.apache.spark.rdd._
import org.apache.spark.rdd.PairRDDFunctions
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._

abstract class GIMV[V:ClassManifest] extends Serializable{

  final def multiply(matrix: RDD[(Int, (Int, V))], vector: RDD[(Int, V)]): RDD[(Int, V)] = {
     val mtxByColum = matrix.map{case(src,(dst,v)) => (dst,(src,v))}

     val tmpMtx = mtxByColum.join(vector).mapValues{case((src,v1),v2) => (src,combine2(v1,v2))}

     val tmpMtxByRow = tmpMtx.map{case(dst,(src,v)) => (src,(dst,v))}

     val newVector = tmpMtxByRow.groupByKey().mapValues(v=>combineAll(v))

     return assign(vector,newVector)
  
  }
  def combine2(v1: V, v2: V): V
  def combineAll(row: Seq[(Int, V)]): V
  def assign(original: RDD[(Int,V)], newv:RDD[(Int,V)]): RDD[(Int,V)]
  def test(v1: Int, v2:Int)
}


