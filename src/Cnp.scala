import scala.collection._
import org.apache.spark.rdd._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._


final class myGIMV extends GIMV[Int]{
   def combine2(v1: Int, v2: Int): Int = {
      //println("in myGIMV.combine2.....")
     return v1 * v2
   }

   def combineAll(row: Seq[(Int,Int)]): Int = {
      return row.map{case(id,v)=>v}.min
   }

   def assign(original: RDD[(Int,Int)], newv: RDD[(Int,Int)]): RDD[(Int, Int)] = {
      return original ++ newv reduceByKey{case(cpn1,cpn2) => if(cpn1 > cpn2) cpn2 else cpn1}
   }

   def test(v1:Int,v2:Int){
      println("~~~~~" + v1 + "~~~~~~~~")
   }
   
}

class myTest{
   var InitTime = 0.0 : Double;
   var call = 0;
   def test(v:Int){
   }
   def init(){
      InitTime = System.nanoTime;
      call = 0;
   }
   def time() =  {
      call = call + 1;
      val t1 = System.nanoTime : Double;
      println("Elapsed time " + (t1 - InitTime) / 1000000.0 + " msecs in " + call + " iterations." );

   }

}

object Cnp {
   def main(args: Array[String]){

      val lines = sc.textFile(args(0), 1)
      val links_ori = lines.map{
	 s => val parts = s.split("\\s+")
	 (parts(0).toInt, parts(1).toInt)
      }.distinct()
      val inv_links = links_ori.map{case (k,v) => (v,k)}
      val all_link = links_ori.union(inv_links)
      val links = all_link;

      val selflink = links.groupByKey().map{case(f,v)=>(f,f)}
      val matrix = links.union(selflink).map{case(src,dst) => (src,(dst,1))}.persist();
      
      var flag = 1L
      var component = selflink
      val size = 32;
      val timer = new myTest();
      val GIMV_solver = new myGIMV();
      timer.init();
      while(flag != 0L){
	 val new_component = GIMV_solver.multiply(matrix, component).coalesce(32,false);
	 val converge = component.join(new_component).values.filter{ case (url1, url2) => url1 != url2};
	 flag = converge.count()	/* if count != 0 -> not converged */
	 component = new_component
         timer.time();
      }
      //println("result:")
      //component.foreach(f => println(f))
      timer.time();
      component.saveAsTextFile(args(1))
      
   }


   /* matrix: (src, (dst, val)) where val == 1 or 0 (reachable or not)
    * vector: (id, val)
    * new_vector: (id, val)
    */
    /*
   def multiply(matrix: RDD[(Int, (Int, Int))], vector: RDD[(Int, Int)]): RDD[(Int,Int)] = {
      val mtxByColum = matrix.map{case(src,(dst,v)) => (dst,(src,v))}
      
      // "val" of each entry in tmpMtx means reachable component id 
      val tmpMtx = mtxByColum.join(vector).mapValues{case((src,v1),v2) => (src,combine2(v1, v2))}

      val tmpMtxByRow = tmpMtx.map{case(dst,(src,v)) => (src,(dst,v))}

      // get the minimum reachable component id through combineAll() within a each row 
      val newVector = combineAll(tmpMtxByRow)

      // compare with original component id and choose the smaller one 
      return assign(vector,newVector)
   }

   def combine2(v1: Int, v2: Int): Int = {
     return v1 * v2
   }

   // matrix: (src,(dst, val)) where val == component id
    
   def combineAll(matrix: RDD[(Int,(Int, Int))]): RDD[(Int, Int)] = {
      val tmp1 = matrix.mapValues{case (dst,cpn) => cpn}
      val tmp2 = tmp1.groupByKey()
      val tmp3 = tmp2.mapValues{case seq => seq.min}
      return tmp3
   }

   def assign(original: RDD[(Int,Int)], newv: RDD[(Int,Int)]): RDD[(Int, Int)] = {
      return original ++ newv reduceByKey{case(cpn1,cpn2) => if(cpn1 > cpn2) cpn2 else cpn1}
   }*/
}
